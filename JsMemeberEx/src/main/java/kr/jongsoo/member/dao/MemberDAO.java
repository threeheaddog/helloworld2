package kr.jongsoo.member.dao;

import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import kr.jongsoo.member.vo.MemberVO;


@Repository
public class MemberDAO {
	
	@Autowired
	private SqlSession sqlSession;
	

	// 저장
	public void insert(MemberVO memberVO) {
		sqlSession.insert("member.insert", memberVO);
	}
	// 레벨변경
	public void updateLevel(String userid, int lev) {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("userid", userid);
		map.put("lev", lev+"");
		sqlSession.update("member.updateLevel", map);
	}
	// 키저장
	public void updateKey(String userid, String key) {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("userid", userid);
		map.put("key", key);
		sqlSession.update("member.updateKey", map);
	}
	// 키읽기
	public String getKey(String userid) {
		return sqlSession.selectOne("member.getKey", userid);
	}
	
	// 아이디 개수 조회
	public int getUseridCount(String userid) {
		return sqlSession.selectOne("member.useridCount", userid);
	}

}
