package kr.jongsoo.member.vo;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@XmlRootElement
public class MemberVO {
	private int	idx;
	private String	userid;
	private String	password;
	private int	lev;
	private Date regdate;
	private String	temp1;
	private String	temp2;
}
