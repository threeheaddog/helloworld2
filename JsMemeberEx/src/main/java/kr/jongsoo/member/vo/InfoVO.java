package kr.jongsoo.member.vo;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@XmlRootElement
public class InfoVO {
	
	private int	idx;
	private String	username;
	private Date birth;
	private String	phone;
	private String	zipcode;
	private String	addr1;
	private String	addr2;
	private Date regdate;
	private Date updatedate;
	private String	temp1;
	private String	temp2;
	
}
