package kr.jongsoo.member;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import kr.jongsoo.member.service.MemberService;

@Controller
public class MemberController {

	@Autowired // 서베스객체 자동 주입
	private MemberService memberService;
	
	@Autowired // 메일 센터 자동 주입
	private JavaMailSenderImpl mailSender;
	

	// 회원 가입 폼
	@RequestMapping(value="/home")
	public String home() {
		return "home";
	}
	/*
	// 회원 가입 폼
	@RequestMapping(value="/signIn")
	public String signIn(HttpServletRequest request, Model model) {
		String referer = "";
		if(request.getParameter("referer")==null)
			referer = request.getHeader("referer");
		else
			referer = request.getParameter("referer");
		
		if(referer==null || referer.trim().length()==0) 
			referer= request.getContextPath() + "/";
		model.addAttribute("prevUrl", referer);
		return "signIn";
	}
	// 로그인 폼
	@RequestMapping(value="/logIn")
	public String logIn(HttpServletRequest request, Model model) {
		String referer = "";
		if(request.getParameter("referer")==null)
			referer = request.getHeader("referer");
		else
			referer = request.getParameter("referer");
		
		if(referer==null || referer.trim().length()==0) 
			referer= request.getContextPath() + "/";
		
		model.addAttribute("prevUrl", referer);
		model.addAttribute("result", request.getParameter("result"));
		return "logIn";
	}
	
	*/
}
